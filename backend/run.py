import endpoints.static_files
import endpoints.start_game
import endpoints.guess_letter
import endpoints.save_score
import endpoints.get_highscores

from app import app

if __name__ == "__main__":
    app.run()
