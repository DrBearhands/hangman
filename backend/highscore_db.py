max_name_length = 16

from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os

_Base = declarative_base()

class _HighScoreEntry(_Base):
    __tablename__ = "highscores"
    id=Column(Integer, primary_key=True)
    score=Column('score', Integer)
    name=Column('name', String(max_name_length))

    def __init__(self, score, name):
        self.score = score
        self.name = name

_session = None


#'postgresql://usr:pwd@db:5432/hangman'
#'postgresql://usr:pwd@localhost:5432/hangman'

usr = os.environ['DB_USERNAME']
pwd = os.environ['DB_PASSWORD']
table = os.environ['DB_NAME']
_engine = create_engine(f'postgresql://{usr}:{pwd}@localhost:5432/{table}')

_Base.metadata.create_all(_engine)

_Base.metadata.bind = _engine

_DB_session = sessionmaker(bind=_engine)

_session = _DB_session()


def add_score(score, name):
    _session.add(_HighScoreEntry(score, name[:max_name_length]))
    _session.commit()

def get_highscores():
    sql_query = '''
        SELECT score, name
        FROM highscores
        ORDER BY score DESC
        LIMIT 20
    '''
    result = _engine.execute(sql_query)
    return [{'score': row[0], 'user': row[1]} for row in result]
