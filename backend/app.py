from flask import Flask, request, url_for, send_from_directory, make_response
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
