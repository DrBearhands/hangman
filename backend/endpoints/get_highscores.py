from app import app
import highscore_db
import json

@app.route("/get_highscores", methods=["GET"])
def get_highscores():
    return json.dumps(highscore_db.get_highscores())

import endpoints
